'''
Created on Jun 9, 2014

@author: hauyeung
'''
import unittest

def title(s):
    "How close is this function to str.title()?"
    return s[0].upper()+s[1:]
class Test(unittest.TestCase):  

    def test_totitle(self):
        self.assertEqual(title("test"), "test".title())
        
    def test_totitlefail(self):
        self.assertEqual( title("fail test"), "fail test".title())


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()